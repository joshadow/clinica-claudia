<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Vaccine extends Model
{
    use HasFactory;

    protected $fillable = ['name','price','active'];

    public function visits()
    {
        return $this->belongsToMany(Visit::class,'visit_vaccines',
                    'vaccine_id','visite_id')->withTimestamps();
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class,'invoice_details',
                    'vaccine_id','invoice_id')
                    ->withPivot('price')
                    ->withPivot('coverage')
                    ->withTimestamps();
    }
}
