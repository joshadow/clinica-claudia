<?php

namespace App\Http\Controllers;

use App\Models\Vaccine;
use Illuminate\Http\Request;
use Inertia\Inertia;

class VaccineController extends Controller
{
    public function index()
    {
        return Inertia::render('Vaccines/All',[
            //'vaccines' => Vaccine::all()
        ]);
    }

    public function all()
    {
        $name = request('name');
        if($name)
        {
            return Vaccine::where('name','like','%'.$name.'%')->orderBy('active','desc')->paginate(10);
        }
        return Vaccine::orderBy('active','desc')->paginate(10);
    }

    public function list()
    {
        return Vaccine::where('active', true)->get();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|unique:vaccines',
            'price' => 'required|numeric',

        ]);
        Vaccine::create(['name' => ucwords(strtolower($request->name)), 'price' => $request->price]);

        return redirect()->route('vaccines.index',$request->insurance_id)->with(['toast' => ['message' => 'Vacuna creada correctamente','success' => true]]);
    }

    public function update(Vaccine $vaccine, Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|unique:vaccines,name,'.$vaccine->id,
            'price' => 'required|numeric',
            'active' => 'required'
        ]);

        $vaccine->update([
            'name' => ucwords(strtolower($request->name)),
            'price' => $request->price,
            'active' => $request->active
        ]);

        return redirect()->route('vaccines.index')->with(['toast' => ['message' => 'Vacuna actualizada correctamente','success' => true]]);
    }

    public function destroy(Vaccine $vaccine)
    {
        $vaccine->delete();
        return redirect()->route('vaccines.index')->with(['toast' => ['message' => 'Vacuna eliminada correctamente','success' => true]]);
    }
}
